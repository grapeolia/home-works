package hw1;

import java.util.Arrays;
import java.util.Scanner;

public class GameGuessRandomNumber {
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    System.out.print("Please enter your name: ");
    String name = scan.next();
    System.out.println(name + ", let the game begin!");

    int minRange = 0;
    int maxRange = 100;

    int generatedNum = generateNum(minRange,maxRange);

    int [] userNumbers = {};
    int userNum = scanUserNumberFromTo(minRange,maxRange);
    userNumbers = addUserNumber(userNumbers, userNum);

    boolean isUserNumEqual = isUserNumEqual(userNum,generatedNum);
    while (!isUserNumEqual){
      boolean isUserNumBigger = isUserNumBigger(userNum,generatedNum);
      if (isUserNumBigger){
        System.out.println("Your number is too big. Please, try again.");
        maxRange = userNum-1;
      } else {
        System.out.println("Your number is too small. Please, try again.");
        minRange = userNum+1;
      }
        userNum = scanUserNumberFromTo(minRange,maxRange);
      userNumbers = addUserNumber(userNumbers,userNum);
      isUserNumEqual = isUserNumEqual(userNum,generatedNum);
    }
    System.out.println("Your numbers: "+Arrays.toString(userNumbers));
    System.out.println("Congratulations, "+name+"!");

  }
  public static int generateNum(int min, int max) {
    return (int) ((Math.random() * (max - min)) + min);
  }

  public static int scanUserNumberFromTo(int min, int max) {
    System.out.println("Enter your number "+min+".."+max);
    Scanner scan = new Scanner(System.in);
    int userNum = scan.hasNextInt()? (int) scan.nextInt() : -1;
    while (userNum<min || userNum>max) {
      System.out.println("Please reenter your number "+min+".."+max);
      scan.nextLine();
      userNum = scan.hasNextInt()? (int) scan.nextInt() : -1;
    }
    return userNum;
  }

  public static boolean isUserNumEqual (int userNum, int generatedNum){
    if(userNum==generatedNum){
      return true;
    }
    return false;
  }

  public static boolean isUserNumBigger(int userNum, int generatedNum){
    if(userNum<generatedNum) {
      return false;
    }
    return true;
  }

  public static int[] addUserNumber(int existingNumbers[], int newNumber){
    int[] updatedNumbers = new int[existingNumbers.length+1];
    for (int i=0; i<existingNumbers.length; i++){
      updatedNumbers[i] = existingNumbers[i];
    }
    updatedNumbers[updatedNumbers.length-1] = newNumber;
    return updatedNumbers;
  }
}

