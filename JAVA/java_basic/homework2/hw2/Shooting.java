package hw2;

import java.util.Arrays;
import java.util.Scanner;

public class Shooting {
  public static void main(String[] args) {

    System.out.println("All set. Get ready to rumble!");
    int size = 5;
    String [][] matrix = generateMatrix(size);
    printMatrix(matrix);
    int [] target = generatePoint(size);
    //System.out.println(Arrays.toString(target));

    int userRowNumber = scanUserTarget(size, "row");
    int userColNumber = scanUserTarget(size, "column");

    boolean isUserTargetCorrect = isUserTargetCorrect(target,userRowNumber,userColNumber);
    while (!isUserTargetCorrect){
      markUserWrongTarget(matrix,userRowNumber,userColNumber);
      printMatrix(matrix);
      System.out.println("The target is wrong! Please try again");
      userRowNumber = scanUserTarget(size, "row");
      userColNumber = scanUserTarget(size, "column");
      isUserTargetCorrect = isUserTargetCorrect(target,userRowNumber,userColNumber);
    }
    markUserCorrectTarget(matrix,userRowNumber,userColNumber);
    printMatrix(matrix);
    System.out.println("You have won!");
  }


  public static String[][] generateMatrix(int size){
    String [][] matrix = new String[size+1][size+1];
    for (int i=0; i<matrix.length; i++){
      for (int j=0; j<matrix.length; j++){
        matrix[i][j] = "-";
        matrix[0][j] = Integer.toString(j);
        matrix[i][0] = Integer.toString(i);
      }
    }
    return matrix;
  }
  public static void printMatrix(String[][] matrix) {
    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix.length; j++) {
        System.out.print(" | "+matrix[i][j]);
      }
      System.out.println(" | ");
    }
  }
  public static int[] generatePoint(int size){
    int [] target = new int[2];
    target[0] = (int) ((Math.random() * (size-1)) + 1);
    target[1] = (int) ((Math.random() * (size-1)) + 1);
    return target;
  }
  public static int scanUserTarget(int max, String targetName) {
    System.out.println("Enter your "+targetName+" number 1.."+max);
    Scanner scan = new Scanner(System.in);
    int userNum = scan.hasNextInt() ? (int) scan.nextInt() : -1;
    while (userNum<1 || userNum>max) {
      System.out.println("Please reenter your "+targetName+" number 1.."+max);
      scan.nextLine();
      userNum = scan.hasNextInt()? (int) scan.nextInt() : -1;
    }
    return userNum;
  }

  public static boolean isUserTargetCorrect(int[] generatedNum, int userRowNum, int userColNum) {
    if (generatedNum[0] == userRowNum && generatedNum[1] == userColNum) {
      return true;
    }
    return false;
  }

  public static void markUserWrongTarget(String[][] matrix, int userRowNum, int userColNum){
    matrix[userRowNum][userColNum] = "*";
  }
  public static void markUserCorrectTarget(String[][] matrix, int userRowNum, int userColNum){
    matrix[userRowNum][userColNum] = "x";
  }
}
