package hw3;

import java.rmi.registry.LocateRegistry;
import java.util.Arrays;
import java.util.Scanner;

public class DailyTasks {
  public static void main(String[] args) {
    String[][] schedule = new String[7][2];
    schedule[0][0] = "Sunday";
    schedule[0][1] = "do home work on Sunday";
    schedule[1][0] = "Monday";
    schedule[1][1] = "go to courses; watch a film on Monday";
    schedule[2][0] = "Tuesday";
    schedule[2][1] = "do home work on Tuesday";
    schedule[3][0] = "Wednesday";
    schedule[3][1] = "go to courses; watch a film on Wednesday";
    schedule[4][0] = "Thursday";
    schedule[4][1] = "do home work on Thursday";
    schedule[5][0] = "Friday";
    schedule[5][1] = "go to courses; watch a film on Friday";
    schedule[6][0] = "Saturday";
    schedule[6][1] = "do home work on Saturday";

    String userDay = scanUserDay();
    int userDayIndex = getUserDayIndex(userDay);

    while (userDayIndex!=7){
      System.out.println("Your tasks for "+userDay+": "+ schedule[userDayIndex][1]);
      userDay = scanUserDay();
      userDayIndex = getUserDayIndex(userDay);
    }


  }

  public static String scanUserDay (){
    Scanner scan = new Scanner(System.in);
    System.out.print("Please, input the day of the week: ");
    String userDay = scan.next().toLowerCase().trim();
    int userDayIndex = getUserDayIndex (userDay);
    while (userDayIndex == -1){
      System.out.println("Sorry, I don't understand you, please try again. '"+userDay+"' doesn't exist");
      System.out.print("Please, input the day of the week: ");
      userDay = scan.next().toLowerCase().trim();
      userDayIndex = getUserDayIndex(userDay);
    }
    return userDay;
  }

  public static int getUserDayIndex(String userDay){
    String[] options = {"sunday","monday","tuesday","wednesday","thursday","friday","saturday","exit"};
    for(int i = 0; i < options.length; i++){
      if (options[i].equals(userDay)){
        return i;
      }
    }
    return -1;
  }
}
