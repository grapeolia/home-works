package hw4;

import java.util.Arrays;
import java.util.Objects;

public class Pet {

  private String species;
  private String nickname;
  private int age;
  private int trickLevel;
  private String[] habits;



  public Pet(){

  }

  public Pet(String species, String nickname){
    this.species = species;
    this.nickname = nickname;
  }

  public Pet(String species, String nickname, int age, int trickLevel, String... habits){
    if (trickLevel < 0 || trickLevel >100) {
      throw new IllegalArgumentException("TrickLevel must be in range 0..100. Please check the value");
    }
    this.trickLevel = trickLevel;
    if (!isParameterInRange(age,0,100)) {
      throw new IllegalArgumentException("Age must be more or equal 0. Please check the value");
    }
    this.age = age;
    this.species = species;
    this.nickname = nickname;
    this.habits = new String[habits.length];
    for (int i=0; i< habits.length; i++) {
      this.habits[i] = habits[i];
    }
  }

  private boolean isParameterInRange(int parameter, int min, int max){
    return parameter >= min && parameter <= max;
  }

  public void eat(){
    System.out.println("Я кушаю!");
  }

  public void respond(){
    System.out.println("Привет, хозяин. Я - "+this.nickname+". Я соскучился!");
  }

  public void foul(){
    System.out.println("Нужно хорошо замести следы...");
  }

  public String getSpecies() {
    return species;
  }

  public void setSpecies(String species) {
    this.species = species;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    if (age < 0 || age >100) {
      throw new IllegalArgumentException("Age must be more or equal than 0. Please check the value");
    }
    this.age = age;
  }

  public int getTrickLevel() {
    return trickLevel;
  }

  public void setTrickLevel(int trickLevel) {
    if (trickLevel < 0 || trickLevel >100) {
      throw new IllegalArgumentException("TrickLevel must be in range 0..100. Please check the value");
    }
    this.trickLevel = trickLevel;
  }

  public String[] getHabits() {
    return habits;
  }

  public void setHabits(String... habits) {
    this.habits = new String[habits.length];
    for (int i=0; i< habits.length; i++) {
      this.habits[i] = habits[i];
    }
  }

  @Override
  public String toString() {
    String petDescription = "";
    petDescription += this.species!=null ? this.species : "";
    petDescription += "{";
    petDescription += this.nickname!=null ? this.nickname : "";
    petDescription += ", age="+this.age;
    petDescription += ", trickLevel="+this.trickLevel;
    petDescription += this.habits!=null ? ", habits="+Arrays.toString(this.habits) : "";
    petDescription +="}";
    return petDescription;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Pet)) return false;
    Pet pet = (Pet) o;
    return age == pet.age && trickLevel == pet.trickLevel && Objects.equals(species, pet.species) && Objects.equals(nickname, pet.nickname) && Arrays.equals(habits, pet.habits);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(species, nickname, age, trickLevel);
    result = 31 * result + Arrays.hashCode(habits);
    return result;
  }
}
