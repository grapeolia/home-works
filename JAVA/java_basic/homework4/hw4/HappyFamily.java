package hw4;

import java.util.Arrays;

public class HappyFamily {
  public static void main(String[] args) {

    Pet chip = new Pet("dog","Chip");
    Pet barsik = new Pet("cat","Barsik",2, 23,"play", "sleep");
    Pet sharik = new Pet();
    sharik.setSpecies("dog");
    sharik.setNickname("Sharik");
    sharik.setAge(2);
    sharik.setTrickLevel(67);
    sharik.setHabits("barking", "playing");

    //parents:
    Human victoriia = new Human("Victoriia","Ivanenko",1982);
    Human ivan = new Human("Ivan","Ivanenko",1980);

    //children:
    Human mariia = new Human("Mariia","Ivanenko", 2010, victoriia , ivan);
    Human petro = new Human("Petro","Ivanenko",2014, victoriia, ivan);
    Human ivanka = new Human("Ivanka","Ivanenko",2018, 89, sharik, victoriia, ivan, new String[][]{{"monday", "cleaning"},{"tuesday","washing"},{"wednesday","studying"}});
    System.out.println("ivanka with all arguments: "+ivanka.toString());

    System.out.println(victoriia.toString());
    System.out.println(ivan.toString());
    System.out.println(mariia.toString());
    System.out.println(petro.toString());

    Family ivanenko = new Family(victoriia,ivan, mariia);
    Family ivanovWithoutChildren = new Family(victoriia,ivan);
    System.out.println("ivanovWithoutPetro "+ivanenko.toString());
    System.out.println(ivanovWithoutChildren.toString());

    ivanenko.addChild(petro);
    System.out.println("ivanovWithPetroAdded "+ivanenko.toString());

    ivanenko.deleteChild(0);
    System.out.println("ivanovWithoutMariia "+ivanenko.toString());

    System.out.println("FamilyGroupOfThree: "+ivanenko.countFamily());

    System.out.println(Arrays.toString(barsik.getHabits()));
    System.out.println(barsik.toString());
    barsik.eat();
    barsik.respond();
    barsik.foul();

  }

  }

