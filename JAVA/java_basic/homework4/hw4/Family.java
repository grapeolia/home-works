package hw4;

import java.util.Arrays;

public class Family {

  private Human mother;
  private Human father;
  private Human[] children;
  private Pet pet;

  public Family(Human mother, Human father, Human... children){
    this.mother = mother;
    this.father = father;
    this.children = new Human[children.length];
    for (int i=0; i< children.length; i++) {
      this.children[i] = children[i];
    }
    this.mother.setFamily(this);
    this.father.setFamily(this);

  }

  public Human[] addChild(Human child){
//    child.setFamily(this);
    Human[] newChildren = new Human[this.children.length+1];
    for (int i=0; i<this.children.length; i++){
      newChildren[i] = this.children[i];
    }
    newChildren[newChildren.length-1] = child;
    this.children = newChildren;
    return this.children;
  }

  public boolean deleteChild(int childRmIndex){
    if(this.children == null || childRmIndex<0 || childRmIndex>= this.children.length){
      return false;
    }
    Human[] newChildren = new Human[this.children.length-1];
    for(int i = 0, k = 0; i < this.children.length; i++){
      if (i == childRmIndex) {
        continue;
      }
      newChildren[k++] = this.children[i];
    }
    this.children = newChildren;
    return true;
  }

  public int countFamily(){
    return 2+this.children.length;
  }

  public Human getMother() {
    return mother;
  }

  public void setMother(Human mother) {
    this.mother = mother;
  }

  public Human getFather() {
    return father;
  }

  public void setFather(Human father) {
    this.father = father;
  }

  public Human[] getChildren() {
    return children;
  }

  public void setChildren(Human[] children) {
    this.children = children;
  }

  public Pet getPet() {
    return pet;
  }

  public void setPet(Pet pet) {
    this.pet = pet;
  }

  @Override
  public String toString() {
    String familyDescr = "Family{";
    if(this.mother!=null){
      familyDescr += " mother: " +this.mother;
    }
    if(this.father!=null){
      familyDescr += " father: " +this.father;
    }
    if(this.children!=null){
      familyDescr += ", children: " +Arrays.toString(this.children);
    }
    if(this.pet!=null){
      familyDescr += ", pet: " +pet.toString();
    }

    return familyDescr;
  }
}
