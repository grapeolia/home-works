package hw4;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Human{

  private String name;
  private String surname;
  private int year;
  private int iq;
  private Pet pet;
  private Human mother;
  private Human father;
  private String[][] schedule;

  private Family family;

  public Human () {

  }

  public Human (String name, String surname, int year){
    this.name = name;
    this.surname = surname;
    if (!isParameterInRange(year,1900,2022)) {
      throw new IllegalArgumentException("Year must be in range 1900..2022. Please check the value");
    }
    this.year = year;
  }

  public Human (String name, String surname, int year, Human mother, Human father) {
    this.name = name;
    this.surname = surname;
    if (!isParameterInRange(year,1900,2022)) {
      throw new IllegalArgumentException("Year must be in range 1900..2022. Please check the value");
    }
    this.year = year;
    this.mother = mother;
    this.father = father;
  }

  public Human (String name, String surname, int year, int iq, Pet pet, Human mother, Human father, String[][] schedule) {
    this.name = name;
    this.surname = surname;
    if (!isParameterInRange(year,1900,2022)) {
      throw new IllegalArgumentException("Year must be in range 1900..2022. Please check the value");
    }
    this.year = year;
    if (!isParameterInRange(iq,0,100)) {
      throw new IllegalArgumentException("Iq must be in range 0..100. Please check the value");
    }
    this.iq = iq;
    this.pet = pet;
    this.mother = mother;
    this.father = father;
    this.schedule = new String[schedule.length][2];
    System.out.println(Arrays.toString(schedule[0]));
    this.schedule = schedule;
//    for (int i=0; i< schedule.length; i++) {
//      this.schedule[i][0] = Arrays.toString(schedule[i][0]);
//    }
    if (!isDayOfTheWeek(this.schedule)) {
      throw new IllegalArgumentException("Please check entered schedule days");
    }
  }

  public void greetPet(){
  System.out.println("Привет, "+ this.pet.getNickname());
  }

  public void describePet(){
    String trickLevel = this.pet.getTrickLevel()<=50 ? "почти не хитрый" : "очень хитрый";
    System.out.println("У меня есть"+this.pet.getSpecies()+" , ему "+this.pet.getAge()+" лет, он "+trickLevel);
  }

  private boolean isParameterInRange(int parameter, int min, int max){
    return parameter >= min && parameter <= max;
  }

  private static boolean isDayOfTheWeek(String[][] schedule){
    String [] daysOfWeek = {"monday","tuesday","wednesday","thursday","friday","saturday","sunday"};
    List<String> listOfDays = Arrays.asList(daysOfWeek);
    for(int i=0; i<schedule.length; i++){
      if (!listOfDays.contains(schedule[i][0].toLowerCase().trim())){
        return false;
      };
    };
    return true;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    if (year < 1900 || year >2022) {
      throw new IllegalArgumentException("Year must be in range 1900..2022. Please check the value");
    }
    this.year = year;
  }

  public int getIq() {
    return iq;
  }

  public void setIq(int iq) {
    if (iq < 0 || iq >100) {
      throw new IllegalArgumentException("Iq must be in range 0..100. Please check the value");
    }
    this.iq = iq;
  }

  public Pet getPet() {
    return pet;
  }

  public void setPet(Pet pet) {
    this.pet = pet;
  }

  public Human getMother() {
    return mother;
  }

  public void setMother(Human mother) {
    this.mother = mother;
  }

  public Human getFather() {
    return father;
  }

  public void setFather(Human father) {
    this.father = father;
  }

  public String[][] getSchedule() {
    return schedule;
  }

  public void setSchedule(String[][] schedule) {
    if (!isDayOfTheWeek(schedule)) {
      throw new IllegalArgumentException("Please check entered schedule days");
    }
    this.schedule = schedule;
  }

  public Family getFamily() {
    return family;
  }

  public void setFamily(Family family) {
    this.family = family;
  }

  @Override
  public String toString() {
    String humanDescription = "Human{";
    humanDescription += this.name!=null ? "name='" + this.name + '\'' : "";
    humanDescription += this.surname!=null ? ", surname='" + this.surname + '\'' : "";
    humanDescription += this.year!=0 ? ", year=" + this.year : "";
    humanDescription += this.iq!=0 ? ", iq=" + this.iq : "";
    humanDescription += this.pet!=null ? ", " + this.pet.toString() : "";
    humanDescription += this.mother!=null ? ", mother " + this.mother.toString() : "";
    humanDescription += this.father!=null ? ", father " + this.father.toString() : "";
    humanDescription += this.schedule!=null ? ", schedule " + Arrays.deepToString(this.schedule) : "";
    humanDescription += "}";
    return humanDescription;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Human)) return false;
    Human human = (Human) o;
    return year == human.year && iq == human.iq && Objects.equals(name, human.name) && Objects.equals(surname, human.surname) && Objects.equals(pet, human.pet) && Objects.equals(mother, human.mother) && Objects.equals(father, human.father) && Arrays.equals(schedule, human.schedule);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(name, surname, year, iq, pet, mother, father);
    result = 31 * result + Arrays.hashCode(schedule);
    return result;
  }
}
