package hw5;

import java.util.Set;

public class Dog extends Pet implements otherPetMethods{
  public Dog (){
    super();
  }
  public Dog (String nickname){
    super(species, nickname);
    species = Species.DOG;

  }
  public Dog (String nickname, int age, int trickLevel, String... habits){
    super(species,nickname,age,trickLevel,habits);
    species = Species.DOG;
  }
  @Override
  void respond() {
    System.out.println("Привет, хозяин. Я - "+this.getSpecies()+" "+this.nickname+". Я соскучился!");
  }

  @Override
  public void foul() {
    System.out.println("Нужно хорошо замести следы...");
  }
}
