package hw5;

import java.util.*;

import static hw5.Human.pet;

public class Family {

  private Woman mother;
  private Man father;
  private List<Child> children;
  private Set<Pet> pets;

  public Family(Woman mother, Man father, Child... children){
    this.mother = mother;
    this.father = father;
    this.children = new ArrayList<Child>();
    Collections.addAll(this.children, children);
    this.mother.setFamily(this);
    this.father.setFamily(this);

  }

  public Set<Pet> getPets(){
    this.pets = new HashSet<Pet>();
    if(this.mother.getPet()!= null){
      this.pets.add(this.mother.getPet());
    }
    if(this.father.getPet()!= null){
      this.pets.add(this.father.getPet());
    }
    if(this.mother.getPet()!= null){
      this.pets.add(this.mother.getPet());
    }
    this.children.forEach(child -> {
      if (child.getPet() != null) this.pets.add(child.getPet());
    });
    return this.pets;
  }
  public List<Child> addChild(Child child){
    if(this.children==null){
      this.children = new ArrayList<Child>();
    }
    this.children.add(child);
    return this.children;
  }

  public boolean deleteChild(int childRmIndex){
    if(this.children == null || childRmIndex<0 || childRmIndex>= this.children.size()){
      return false;
    }
    this.children.remove(childRmIndex);
      return true;
  }

  public boolean deleteChild (Child childToRM){
    if(this.children == null || !this.children.contains(childToRM)){
      return false;
    }
    this.children.remove(childToRM);
    return true;
  }

  public int countFamily(){
    int members = (int) this.children.stream().count();
    members += (this.mother != null) ? 1: 0;
    members += (this.father != null) ? 1: 0;
    return members;
  }

  public Woman getMother() {
    return mother;
  }

  public void setMother(Woman mother) {
    this.mother = mother;
  }

  public Man getFather() {
    return father;
  }

  public void setFather(Man father) {
    this.father = father;
  }

  public List<Child> getChildren() {
    return children;
  }

  public void setChildren(List<Child> children) {
    this.children = children;
  }

  public void setPets(Set<Pet> pets) {
    this.pets = pets;
  }

  @Override
  public String toString() {
    String familyDescr = "Family{";
    if(this.mother!=null){
      familyDescr += " mother: " +this.mother;
    }
    if(this.father!=null){
      familyDescr += " father: " +this.father;
    }
    if(this.children!=null){
      familyDescr += ", children: " +this.children;
    }
    if(this.pets!=null){
      familyDescr += ", pet: " +this.pets;
    }

    return familyDescr;
  }

  @Override
  protected void finalize() throws Throwable {
    System.out.println("Calling finalize() method of Object class: "+ this);
    super.finalize();
  }
}
