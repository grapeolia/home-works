package hw5;

import java.util.Set;

public class DomesticCat extends Pet implements otherPetMethods{

  public DomesticCat (){
    super();
  }
  public DomesticCat (String nickname){
    super(species, nickname);
    species = Species.DOMESTIC_CAT;

  }
  public DomesticCat (String nickname, int age, int trickLevel, String... habits){
    super(species,nickname,age,trickLevel,habits);
    species = Species.DOMESTIC_CAT;
  }
  @Override
  void respond() {
    System.out.println("Привет, хозяин. Я - "+this.getSpecies()+" "+this.nickname+". Я соскучился!");
  }

  @Override
  public void foul() {
    System.out.println("Нужно хорошо замести следы...");
  }
}
