package hw5;

import java.util.Map;

public class Man extends Human{
  public Man (){
    super();
  }
  public Man (String name, String surname, int year){
    super(name,surname,year);
  }
  public Man (String name, String surname, int year, Woman mother, Man father){
    super(name,surname,year,mother,father);
  }
  public Man (String name, String surname, int year, int iq, Pet pet, Woman mother, Man father, Map<DayOfWeek,String> schedule){
    super(name,surname,year,iq,pet,mother,father,schedule);
  }

  public void repairCar(){
    System.out.println("I'm "+this.getName()+" and I'm repairing a car");
  }
  @Override
  void greetPet() {
    System.out.println("Привет, "+ this.pet.getNickname());
  }
}
