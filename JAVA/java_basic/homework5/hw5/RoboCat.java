package hw5;

import java.util.Set;

public class RoboCat extends Pet implements otherPetMethods{
  public RoboCat (){
    super();
  }
  public RoboCat (String nickname){
    super(species, nickname);
    species = Species.ROBOCAT;

  }
  public RoboCat (String nickname, int age, int trickLevel, String... habits){
    super(species,nickname,age,trickLevel,habits);
    species = Species.ROBOCAT;
  }
  @Override
  void respond() {
    System.out.println("Привет, хозяин. Я - "+this.getSpecies()+" "+this.nickname+". Я соскучился!");
  }

  @Override
  public void foul() {
    System.out.println("Нужно хорошо замести следы...");
  }
}
