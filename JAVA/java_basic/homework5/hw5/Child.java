package hw5;

import java.util.Map;

public class Child extends Human{
  public Child (){
    super();
  }
  public Child (String name, String surname, int year){
    super(name,surname,year);
  }
  public Child (String name, String surname, int year, Woman mother, Man father){
    super(name,surname,year,mother,father);
  }
  public Child (String name, String surname, int year, int iq, Pet pet, Woman mother, Man father, Map<Human.DayOfWeek,String> schedule){
    super(name,surname,year,iq,pet,mother,father,schedule);
  }

  @Override
  void greetPet() {
    System.out.println("Привет, "+ this.pet.getNickname());
  }
}
