package hw5;

import java.util.*;

public abstract class Pet {

  public static Species species;
  public static String nickname;
  public static int age;
  public static int trickLevel;
  public static Set<String> habits;

  protected Pet(){
  }
  protected Pet(Species species, String nickname){
    this.nickname = nickname;
  }
  protected Pet(Species species, String nickname, int age, int trickLevel, String... habits){
    if (trickLevel < 0 || trickLevel >100) {
      throw new IllegalArgumentException("TrickLevel must be in range 0..100. Please check the value");
    }
    this.trickLevel = trickLevel;
    if (!isParameterInRange(age,0,100)) {
      throw new IllegalArgumentException("Age must be more or equal 0. Please check the value");
    }
    this.age = age;
    this.nickname = nickname;
    Pet.habits = new HashSet<>();
    Collections.addAll(Pet.habits, habits);
  }
  public enum Species {
    CAT,
    DOG,
    ROBOCAT,
    FISH,
    DOMESTIC_CAT,
    PARROT,
    RABBIT,
    UNKNOWN;
  }

  private boolean isParameterInRange(int parameter, int min, int max){
    return parameter >= min && parameter <= max;
  }
  public void eat(){
    System.out.println("Я кушаю!");
  }
  abstract void respond();

  interface methods {
    void foul();
  }
//  public void foul(){
//    System.out.println("Нужно хорошо замести следы...");
//  }

  public Species getSpecies() {
    return species;
  }

  public void setSpecies(Species species) {
    this.species = species;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    if (age < 0 || age >100) {
      throw new IllegalArgumentException("Age must be more or equal than 0. Please check the value");
    }
    this.age = age;
  }

  public int getTrickLevel() {
    return trickLevel;
  }

  public void setTrickLevel(int trickLevel) {
    if (trickLevel < 0 || trickLevel >100) {
      throw new IllegalArgumentException("TrickLevel must be in range 0..100. Please check the value");
    }
    this.trickLevel = trickLevel;
  }

  public Set<String> getHabits() {
    return habits;
  }

  public void setHabits(String... habits) {
    Collections.addAll(this.habits, habits);
  }

  @Override
  public String toString() {
    String petDescription = "";
    petDescription += this.species!=null ? this.species : "";
    petDescription += "{";
    petDescription += this.nickname!=null ? this.nickname : "";
    petDescription += ", age="+this.age;
    petDescription += ", trickLevel="+this.trickLevel;
    petDescription += this.habits!=null ? ", habits="+this.habits : "";
    petDescription +="}";
    return petDescription;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Pet)) return false;
    Pet pet = (Pet) o;
    return age == pet.age && trickLevel == pet.trickLevel && Objects.equals(species, pet.species) && Objects.equals(nickname, pet.nickname) && habits.equals(pet.habits);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(species, nickname, age, trickLevel);
    result = 31 * result + habits.hashCode();
    return result;
  }

  @Override
  protected void finalize() throws Throwable {
    System.out.println("Calling finalize() method of Object class: "+ this);
    super.finalize();
  }


}
