package hw5;

public class UnknownPet extends Pet {

  public UnknownPet (){
    super();
  }
  public UnknownPet (String nickname){
    super(species, nickname);
    species = Species.UNKNOWN;

  }
  public UnknownPet (String nickname, int age, int trickLevel, String... habits){
    super(species,nickname,age,trickLevel,habits);
    species = Pet.Species.UNKNOWN;
  }
  @Override
  void respond() {
    System.out.println("Привет, хозяин. Я - "+this.nickname+". Я соскучился!");
  }

}
