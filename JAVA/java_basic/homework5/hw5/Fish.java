package hw5;

import java.util.Set;

public class Fish extends Pet {

  public Fish (){
    super();
  }
  public Fish (String nickname){
    super(species, nickname);
    species = Species.FISH;

  }
  public Fish (String nickname, int age, int trickLevel, String... habits){
    super(species,nickname,age,trickLevel,habits);
    species = Species.FISH;
  }

  @Override
  void respond() {
    System.out.println("Привет, хозяин. Я - "+this.getSpecies()+" "+this.nickname+". Я соскучился!");
  }

}
