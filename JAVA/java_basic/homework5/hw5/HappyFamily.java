package hw5;

import java.util.Arrays;
import java.util.HashMap;

public class HappyFamily {
  public static void main(String[] args) throws Throwable {

    Pet chip = new DomesticCat("Chip");
    Pet barsik = new Dog("Barsik",2, 23,"play", "sleep");
    Pet roman = new Fish();
    roman.setSpecies(Pet.Species.DOG);
    roman.setNickname("Sharik");
    roman.setAge(2);
    roman.setTrickLevel(67);


    //parents:
    Woman victoriia = new Woman("Victoriia","Ivanenko",1982);
    Man ivan = new Man("Ivan","Ivanenko",1980);

    //children:
    Child mariia = new Child("Mariia","Ivanenko", 2010,victoriia , ivan);
    Child petro = new Child("Petro","Ivanenko",2014, victoriia, ivan);
    Child ivanka = new Child("Ivanka","Ivanenko",2018, 89, roman, victoriia, ivan, new HashMap<>() {{put(Human.DayOfWeek.MONDAY, "cleaning");put(Human.DayOfWeek.TUESDAY,"washing");}});
    Family ivanenko = new Family(victoriia,ivan,mariia);
    ivanenko.addChild(mariia);

    int capacity = 5;
    Family[] allFamilies = new Family[capacity];
    for (int i=0, k=0; i<capacity; i++, k++){
      allFamilies[i] = new Family(new Woman(),new Man(),new Child());
      if(i==capacity-1 && k<=capacity){
        allFamilies[0] = null;
        Family[] newAllHumans = Arrays.copyOfRange(allFamilies,1,allFamilies.length);
        System.out.println("k="+k+"  newAllHumans="+Arrays.deepToString(newAllHumans));
        System.gc();
      }
    }
    System.out.println(Arrays.deepToString(allFamilies));

    //family
    Family ivanenkoWithoutChildren = new Family(victoriia,ivan);
    ivanenko.addChild(petro);

  }

  }

