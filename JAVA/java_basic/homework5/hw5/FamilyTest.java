package hw5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FamilyTest {
  Woman mother = new Woman();
  Man father = new Man();
  Child child = new Child();
  Child childToRm = new Child();
  Child childToAdd = new Child();
  Family family = new Family(mother,father,child,childToRm);

  @Test
  void addChild() {
    assertEquals(family.addChild(childToAdd),this.family.getChildren());
    assertEquals(family.getChildren().equals(childToAdd),childToAdd);
  }

  @Test
  void deleteChildByChildObject() {
    assertTrue(family.deleteChild(childToRm));
    assertFalse(family.deleteChild(childToRm));
  }

  @Test
  void testDeleteChildByIndex() {
    assertTrue(family.deleteChild(1));
    assertFalse(family.deleteChild(1));

  }

  @Test
  void countFamily() {
    assertEquals(family.countFamily(), 4);
  }
}
