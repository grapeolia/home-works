package hw5;

import java.util.Map;

public class Woman extends Human{
  public Woman(){
    super();
  }
  public Woman (String name, String surname, int year){
    super(name,surname,year);
  }
  public Woman (String name, String surname, int year, Woman mother, Man father){
    super(name,surname,year,mother,father);
  }

  public void makeup(String... costetics){
    String womanSpeech = "I'm wearing make-up, I used ";
    for (int i=0; i< costetics.length; i++) {
      womanSpeech += costetics[i];
    }
    System.out.println(womanSpeech);
  }

  @Override
  void greetPet() {
    System.out.println("Привет, "+ this.pet.getNickname());
  }

}
