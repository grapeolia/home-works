package hw5;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {

  @org.junit.jupiter.api.Test
  void testToString() {
    String result = "CAT{Barsik, age=2, trickLevel=23, habits=[play, sleep]}";
    Pet myInstance = new Fish("Barsik",2, 23,"play", "sleep");
    assertEquals(myInstance.toString(), result);

  }
}
