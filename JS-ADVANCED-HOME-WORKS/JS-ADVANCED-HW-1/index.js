class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get name() {
        return this._name;
    }
    set name(value) {
        if (value.length < 4) {
            console.log('The name is too short');
            return;
        }
        this._name = value;
    }
    get age() {
        return this._age;
    }
    set age(value) {
        if (value >= 10 && value <= 100) {
            this._age = value;
        } else {
            console.log('The age is incorrect');
        }
    }
    get salary() {
        return this._salary;
    }
    set salary(value) {
        this._salary = value;
    }

}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang;
    }
    get salary() {
        return this._salary;
    }
    set salary(value) {
        this._salary = value * 3;
    }
    get lang() {
        return this._lang;
    }
    set lang(value) {
        let languages = '';
        value.forEach(element => {
            languages += element + ' ';
        });
        this._lang = languages;
    }
}

let progmammerPetr = new Programmer('Petr', 30, 3000, ['eng', 'ukr']);
let progmammerIvan = new Programmer('Ivan', 28, 5000, ['pol']);
let progmammerVova = new Programmer('Vova', 19, 2000, ['pol']);

console.log(progmammerPetr);
console.log(progmammerIvan);
console.log(progmammerVova);