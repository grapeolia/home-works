class PositionElements {
    constructor(positionData) {
        this.positionData = positionData
    }
    renderElements() {
        const positionFullElem = document.querySelector('.position');
        positionFullElem.innerHTML = '';
        for (let elem in this.positionData) {
            const positionElem = document.createElement('div');
            positionElem.innerHTML = `${elem.charAt(0).toUpperCase()+elem.slice(1)}: </br> ${this.positionData[elem]}`
            positionFullElem.append(positionElem);
        }
        return positionFullElem;
    }
}

export default PositionElements