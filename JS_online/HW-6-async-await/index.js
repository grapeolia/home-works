// континент, країна, регіон, місто, район

import PositionElements from './js/position-elem.js'

async function getPosition() {
    const response = await fetch(`https://api.ipify.org?format=json`);
    const userPosition = await response.json();
    const positionResonese = await fetch(`http://ip-api.com/json/${userPosition.ip}`);
    const positionDetails = await positionResonese.json();
    const continentCountry = positionDetails.timezone.split('/');
    const positionData = {
        continent: continentCountry[0],
        country: positionDetails.country,
        region: positionDetails.region,
        city: positionDetails.city,
        district: positionDetails.regionName
    }

    const positionInElem = new PositionElements(positionData);
    positionInElem.renderElements();
}


const button = document.querySelector('.btn');
button.addEventListener('click', () => {
    getPosition();
})