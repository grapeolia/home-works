"use strict";

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary
    }
    get name() {
        return this._name;
    }
    set name(newName) {
        this._name = newName;
    }
    get age() {
        return this._age;
    }
    set age(newAge) {
        this._age = newAge;
    }
    get salary() {
        return this._salary;
    }
    set salary(newSalary) {
        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }
    get salary() {
        return this._salary * 3;
    }
    set lang(newLang) {
        newLang.forEach((lang) => { this._lang.push(lang) });
    }
    get lang() {
        return this._lang;
    }
}

const ivan = new Employee("Ivan", 27, 15000);
console.log(ivan);

const olena = new Programmer("Olena", 23, 20000, ['eng', 'rus']);
console.log(olena);
console.log(olena.salary);

olena.lang = ['ukr', 'esp'];
console.log(olena.lang);