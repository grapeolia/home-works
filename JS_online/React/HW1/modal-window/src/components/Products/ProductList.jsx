import React, {useEffect, useState} from "react";
import ProductCard from "./ProductCard";

export default function ProductList ({isLoading, products, toggleModal,toggleFavoriteProduct, toggleProductInCart}) {
        
        useEffect(()=>{
            if(!products || products.lenght===0){
                
            }
        },[products])

        return(
            <>
            {isLoading ? (
                <p>Loading...</p>
            ) : (
                <ul className="products-block">
                    {Array.isArray(products) && 
                        products.map((product) => {
                        return (
                            <ProductCard
                            key={product.id+'inFullList'} 
                            product={product}
                            toggleModal={toggleModal}
                            toggleFavoriteProduct={toggleFavoriteProduct}
                            toggleProductInCart={toggleProductInCart}
                            />
                        ) 
                    })}
                </ul>
                )}
            </>
        )
    
}