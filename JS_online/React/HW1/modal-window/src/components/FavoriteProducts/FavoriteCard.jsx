import React, {Component} from "react";
import './FavoriteProducts.scss';

export default function FavoriteCard ({id, name, price, pictureUrl,toggleProductInLocalStorageFavorite}) {
        return(
            <>
                <li>
                    <div className="favorites-block-item">
                        <div className="favourite-product">
                            <span className="favourite-product__price">$ {price}</span>
                            <span className="favourite-product__name">{name}</span>
                            <img className="favourite-product__image" src={pictureUrl} alt={name} />
                        </div>
                        <button onClick={()=>toggleProductInLocalStorageFavorite(id)} className="favorites-block-item__btn-rm">X</button>
                    </div>
                </li>
            </>
        )
    }
