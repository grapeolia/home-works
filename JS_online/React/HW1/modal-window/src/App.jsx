import './style.scss';
import Modal from './components/Modal/Modal';
import React, {useState, useEffect} from 'react';
import { Route, Routes } from 'react-router-dom';
import { getAllProducts } from './api/goods';
import Header from './Header/Header';
import Home from './pages/Home';
import Favorities from './pages/Favorities';
import Cart from './pages/Cart/Cart';
import Error from './pages/Error';

const App =  () => {

  const [currentModalWindow, setCurrentModalWindow] = useState({
    isModalShown: false,
    modalId: null,
    actionOnOk: null,
    paramsForAction: null,
  });

  const [allProducts, setAllProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isCartOpened, setIsCartOpened] = useState(false);

  useEffect(() => {
    getAllProducts()
      .then(products => setAllProducts(products))
    .finally(() => {
      setIsLoading(false)
    })
  }, [])

  const toggleModal = (
    modalId=null, 
    actionOnOk=null,
    paramsForAction=null,
    text=''
    ) => {
    setCurrentModalWindow({
      isModalShown: !currentModalWindow.isModalShown,
      modalId: (!currentModalWindow.isModalShown) ? modalId : "",
      actionOnOk: actionOnOk,
      paramsForAction: paramsForAction,
    })
  };

  let favoriteProducts = JSON.parse(localStorage.getItem('favoriteProducts')) || [];
  let productsInCart = JSON.parse(localStorage.getItem('productsInCart')) || [];

  const toggleFavoriteProduct = (product) => {
    let isProductFavorite = favoriteProducts.length !== 0 ? favoriteProducts.find(item => item.id === product.id) : false;
    if (!isProductFavorite) {
      favoriteProducts = [...favoriteProducts, product]
    } else {
      favoriteProducts = favoriteProducts.filter(item => item.id !== product.id)
    };
    setAllProducts(allProducts.map(item => item.id === product.id ? {...item, isFavorite: !isProductFavorite} : item))
    localStorage.setItem('favoriteProducts', JSON.stringify(favoriteProducts));
  }

  const toggleProductInCart = (product) => {
    let isProductInCart = productsInCart.length !== 0 ? productsInCart.find(item => item.id === product.id) : false;
    if (!isProductInCart) {
      productsInCart = [...productsInCart, product]
    } else {
      productsInCart = productsInCart.filter(item => item.id !== product.id)
    };
    setAllProducts(allProducts.map(item => item.id === product.id ? {...item, isInCart: !isProductInCart} : item))
    localStorage.setItem('productsInCart', JSON.stringify(productsInCart));
  }

  const toggleCartShowing = (show=false) => {
    setIsCartOpened(show)
  }

    return (
      <>
      {currentModalWindow.isModalShown &&
      <Modal 
      {...currentModalWindow}
      toggleModal={toggleModal}
      />
      }
      <Header
      toggleProductInCart={toggleProductInCart}
      toggleCartShowing={toggleCartShowing}
      isCartOpened={isCartOpened}
      productsInCart={productsInCart}
      favoriteProducts={favoriteProducts}
      />
      
      <Routes>
        <Route path="/" element={
          <Home
            isLoading={isLoading}
            products={allProducts}
            toggleModal={toggleModal}
            toggleFavoriteProduct={toggleFavoriteProduct}
            toggleProductInCart={toggleProductInCart}
          />
        }
        />
        <Route path="/favorities" element={
          <Favorities
            isLoading={isLoading}
            products={favoriteProducts}
            toggleModal={toggleModal}
            toggleFavoriteProduct={toggleFavoriteProduct}
            toggleProductInCart={toggleProductInCart}
          />
        }
        />
        <Route path="/cart" element={
          <Cart
            isLoading={isLoading}
            products={productsInCart}
            toggleModal={toggleModal}
            toggleFavoriteProduct={toggleFavoriteProduct}
            toggleProductInCart={toggleProductInCart}
          />
        }
        />
        <Route path="*" element={<Error />}/>
      </Routes>
      
    </>
  );
  
}

export default App;
