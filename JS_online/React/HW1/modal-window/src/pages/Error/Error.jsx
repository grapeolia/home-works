import React from 'react'
import { Link } from 'react-router-dom'

const Error = () => {
  return (
    <>
        <h2>Page Not Found</h2>
        <p>You can go to <Link to='/'>homepage</Link></p>
    </>
  )
}

export default Error
