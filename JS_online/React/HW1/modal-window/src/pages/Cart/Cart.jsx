import React from 'react';
import ProductList from '../../components/Products/ProductList';

const Cart = ({isLoading, products, toggleModal, toggleFavoriteProduct, toggleProductInCart}) => {
    return(
        <ProductList
          isLoading={isLoading}
          products={products}
          toggleModal={toggleModal}
          toggleFavoriteProduct={toggleFavoriteProduct}
          toggleProductInCart={toggleProductInCart}
        />
    )
}

export default Cart