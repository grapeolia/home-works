import React from "react";


export default function NavigationPanel ({productsInCart, favoriteProducts}) {

        return(
            <>
                <div className="navigationPanel container">
                    <div className="navigationPanel__mainLogo">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.054" height="22.018" enableBackground="new 0 0 12.054 22.018" viewBox="0 0 12.054 22.018"><path fill="#231f20" fillRule="evenodd" d="M5.946,4.84C5.627,5.379,5.185,5.714,4.698,5.714   c-0.972,0-1.764-1.332-1.764-2.964c0-0.202,0.012-0.399,0.035-0.589h0.212C3.17,2.254,3.158,2.353,3.146,2.458   C2.922,4.399,3.633,5.397,4.599,5.561C5.394,5.695,5.69,5.086,5.946,4.84z" clipRule="evenodd"/><path fill="#231f20" fillRule="evenodd" d="M7.102 1.345c2.494.225 3.773.951 3.773 1.397 0 .445-1.979.876-5.139.876C2.577 3.618 0 3.225 0 2.742c0-.297.96-.781 2.435-1.11C2.826.717 3.812 0 4.841 0 5.864 0 6.657.584 7.102 1.345zM3.668 11.514c.149 0 1.649-1.113 1.69-2.521C5.407 7.296 6.33 5.208 6.784 5.267c.379.048-.408.778-.039.991.718.414 2.268.648 2.764 1.394-.667 2.835-2.059 5.466-1.879 8.323.066 1.051.93 2.156 1.924 3.384.695.858 1.559 1.75 2.5 2.659H.518c1.176-2.336 2.087-4.731 2.63-6.424.277-.867.225-2.458.124-2.721-.354-.934-1.727-1.585-1.407-2.892C2.005 9.401 2.386 8.013 2.716 7.27c.403-.907.909-1.254 1.376-1.387.165-.046-1.316 2.052-1.378 3.466C2.673 10.317 3.579 11.514 3.668 11.514L3.668 11.514zM9.169 18.768l-.271-.351C8.976 18.522 9.067 18.643 9.169 18.768z" clipRule="evenodd"/></svg>
                        <p className="navigationPanel__mainLogo-name">Store</p>
                    </div>
                    <ul className="navigationPanel__nav">
                        <li className="navigationPanel__nav-item"><a href="/">Home</a></li>
                        <li className="navigationPanel__nav-item"><a href="/cart">Cart ({productsInCart.length})</a></li>
                        <li className="navigationPanel__nav-item"><a href="/favorities">Favourite ({favoriteProducts.length})</a></li>
                    </ul>
                </div>
            </>
        )
    
}