import React from "react";
import HeadPanel from "./HeadPanel";
import NavigationPanel from "./NavigationPanel";
import Advertisement from "./Advertisement";
import './Header.scss'

export default function Header ({toggleProductInCart, toggleCartShowing, isCartOpened, productsInCart, favoriteProducts}) {

        return(
            <>
                <HeadPanel
                toggleProductInCart={toggleProductInCart}
                toggleCartShowing={toggleCartShowing}
                isCartOpened = {isCartOpened}
                productsInCart={productsInCart}
                favoriteProducts={favoriteProducts}
                />
                <NavigationPanel
                productsInCart={productsInCart}
                favoriteProducts={favoriteProducts}
                />
                <Advertisement/>
            </>
        )
    
}