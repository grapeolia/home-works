import React from "react";
import CartItem from "./CartItem";
import SocialMedia from "./SocialMedia";
import UsersCredentials from "./UsersCredentials";

export default function HeadPanel ({isCartOpened, toggleProductInCart, toggleCartShowing, productsInCart}) {

        return(
            <>
                <div className="headPanel">
                    <div className="container">
                        <SocialMedia/>
                        <div className="userInfo">
                            <UsersCredentials/>
                            <CartItem
                            isCartOpened={isCartOpened}
                            toggleProductInCart={toggleProductInCart}
                            toggleCartShowing={toggleCartShowing}
                            productsInCart={productsInCart}
                            />
                        </div>
                    </div>
                </div>
                
            </>
        )
    
}