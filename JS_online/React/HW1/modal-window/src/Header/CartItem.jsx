import React from "react";
import CartListForPopUp from "../components/Cart/CartListForPopUp";

export default function CartItem ({toggleCartShowing, isCartOpened, toggleProductInCart, productsInCart}) {

        return(
            <>
                <div className="cartItem" onMouseOver={() => toggleCartShowing(true)}>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M27.854 7.273c-.321-.446-.821-.702-1.371-.702H9.832c-.012 0-.021.006-.033.007L9.206 4.27C9.014 3.522 8.34 3 7.568 3H4.325c-.276 0-.5.224-.5.5S4.049 4 4.325 4h3.243c.315 0 .59.213.669.519l4.418 17.208c.192.748.865 1.271 1.637 1.271h10.764c.276 0 .5-.224.5-.5s-.224-.5-.5-.5H14.292c-.315 0-.59-.214-.668-.519l-.82-3.193h10.901c.729 0 1.373-.465 1.604-1.156l2.778-8.333C28.261 8.275 28.176 7.72 27.854 7.273zM23.706 17.286H12.547l-2.494-9.715h16.43c.225 0 .429.104.56.287.131.183.166.409.095.622l-.554 1.663H17.42c-.276 0-.5.224-.5.5s.224.5.5.5h8.831l-.857 2.571h-6.783c-.276 0-.5.224-.5.5s.224.5.5.5h6.449l-.7 2.099C24.267 17.097 24.003 17.286 23.706 17.286zM14.444 24.429c-1.26 0-2.286 1.025-2.286 2.285 0 1.261 1.025 2.286 2.286 2.286s2.286-1.025 2.286-2.286C16.73 25.454 15.705 24.429 14.444 24.429zM14.444 28c-.709 0-1.286-.577-1.286-1.286s.577-1.285 1.286-1.285 1.286.576 1.286 1.285S15.153 28 14.444 28zM23.968 24.429c-1.26 0-2.286 1.025-2.286 2.285 0 1.261 1.025 2.286 2.286 2.286s2.286-1.025 2.286-2.286C26.254 25.454 25.229 24.429 23.968 24.429zM23.968 28c-.709 0-1.286-.577-1.286-1.286s.577-1.285 1.286-1.285 1.286.576 1.286 1.285S24.677 28 23.968 28z"/></svg>
                    <p>Cart ({productsInCart.length})</p>
                    {isCartOpened && 
                    <CartListForPopUp
                    productsInCart = {productsInCart}
                    toggleCartShowing={toggleCartShowing}
                    toggleProductInCart={toggleProductInCart} 
                    />
                    }
                </div>
            </>
        )
    
}