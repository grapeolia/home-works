"use strict";

const books = [{
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const rootDiv = document.createElement('div');
document.body.prepend(rootDiv);
rootDiv.setAttribute('id', 'root');

const mustProperties = ['name', 'author', 'price'];

addBooks(books, mustProperties, rootDiv);

function addBooks(books, mustProperties, placeToAdd) {
    placeToAdd.insertAdjacentHTML('beforeend', '<h2>BOOKS</h2>')
    const listOfBooks = document.createElement('ul');
    placeToAdd.insertAdjacentElement('beforeend', listOfBooks)
    let bookNumber = 0;
    books.forEach((book) => {
        bookNumber++;
        try {
            validateBook(book, mustProperties);
            const bookElem = document.createElement('li');
            const bookDescrList = document.createElement('ul');
            listOfBooks.append(bookElem);
            bookElem.innerHTML = `Book ${bookNumber}`;
            bookElem.append(bookDescrList);
            mustProperties.map(descrKey => `<li>${descrKey}: ${book[descrKey]}</li>`)
                .forEach(value => bookDescrList.insertAdjacentHTML('beforeend', value))
        } catch (error) {
            console.error('Book ' + bookNumber + ' ' + error);
        }
    });
}

function validateBook(book, mustProperties) {
    const missingProperties = mustProperties.filter(property => !book.hasOwnProperty(property))
    if (!missingProperties.length) {
        return true;
    }
    throw new Error('has missing properties: ' + missingProperties);
}