'use strict';
console.log('TASK_3');

const user1 = {
    name: "John",
    years: 30
};

let { name: імя, years: вік, isAdmin = false } = user1;
console.log(імя);
console.log(вік);
console.log(isAdmin);