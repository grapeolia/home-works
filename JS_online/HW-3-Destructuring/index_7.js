"use strict";
console.log('TASK_7');

const array = ['value', () => 'showValue'];

// Допишіть код тут
const [value, showValue] = array;

alert(value); // має бути виведено 'value'
alert(showValue()); // має бути виведено 'showValue'