'use strict';
console.log('TASK_1');

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const clientsAll = [...new Set([...clients1, ...clients2])];
console.log(clientsAll);