import Card from './js/card.js';

const rootDiv = document.querySelector('.root');

async function displayCards() {
    const response = await fetch(`https://ajax.test-danit.com/api/json/users`)
    const users = await response.json();
    users.forEach(async user => {
        const { name, username, id: uniqueUserId } = user;
        const response = await fetch(`https://ajax.test-danit.com/api/json/posts`)
        const posts = await response.json();
        const userPosts = posts.filter(post => post.userId === uniqueUserId)
        userPosts.forEach(post => {
            const { id: cardId, title, body } = post;
            const card = new Card(name, username, cardId, title, body);
            rootDiv.append(card.render())

        })
    })
}

displayCards()