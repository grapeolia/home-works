$(document).ready(function() {
    let currentImg;
    let newAuthorPicture;

    for (let n = 14; n <= ($('.our-work-images>*:last-child')).index() + 1; n++) {
        $(`.our-work-images>img:nth-child(${n})`).css({ display: 'none' });
    };

    $('.service-nav-item a').on('click', function(e) {
        e.preventDefault();
        $('.service-content>div').css({ display: 'none' });
        $('.service-nav-item a').removeClass('service-nav-a-clicked')
        let serviceType = $(this).attr('href').substr(1);
        $(`div[data-service-type="${serviceType}"]`).css({ display: 'flex' });
        $(this).addClass('service-nav-a-clicked');
    });


    $('.nav-our-work-item').on('click', function() {
        $(document.querySelector('.nav-our-work-item-clicked')).removeClass('nav-our-work-item-clicked');
        $(this).addClass('nav-our-work-item-clicked');
        displayImgAsPerCategory();
    });

    $('.our-work-images').on('mouseover', function(e) {
        let replacementBlock = document.querySelector('.replacement-block');
        let img;
        if (e.target.tagName === 'IMG' && !e.target.closest('button')) {
            img = e.target;
        } else return;

        changeImgToBlock(img, replacementBlock);

        function changeImgToBlock(img, newElem) {
            if (newElem.style.display !== 'none') {
                newElem.style.display = 'none';
            }
            let category = displayImgAsPerCategory();
            if (!!currentImg && $(currentImg).attr('data-category') === category) {
                currentImg.style.display = 'block';
            }
            currentImg = img;
            newElem.style.display = 'flex';
            currentImg.after(newElem);
            currentImg.style.display = 'none';
            return currentImg;
        }
    });

    $('.our-work-btn').on('click', function() {
        let replacementBlock = document.querySelector('.replacement-block');
        replacementBlock.style.display = 'none';
        let scrollTo = $(this).offset().top - $(window).height();
        console.log(scrollTo);
        $('html, body').stop().animate({
            scrollTop: scrollTo
        }, 1000);
        $(this).css({ display: 'none' });

        displayImgAsPerCategory();
    });

    document.querySelector('.prev').onclick = function() {
        if ($(document.querySelector('.personal-photo-clicked')).index() === 0) {
            newAuthorPicture = document.querySelector('.pictures img:last-child');
        } else {
            newAuthorPicture = document.querySelector('.personal-photo-clicked').previousElementSibling;
        }
        makeHigherAuthorPicture();
        moveToNeededItem();

    };

    document.querySelector('.next').onclick = function() {
        if ($(document.querySelector('.personal-photo-clicked')).index() === $(document.querySelector('.pictures').lastChild).index() - 1) {
            newAuthorPicture = document.querySelector('.pictures img:first-child');

        } else {
            newAuthorPicture = document.querySelector('.personal-photo-clicked').nextElementSibling;
        }
        makeHigherAuthorPicture();
        moveToNeededItem();
    };

    document.querySelector('.pictures').onclick = function(e) {
        if (e.target.tagName !== 'IMG') return;
        newAuthorPicture = e.target;
        makeHigherAuthorPicture();
        moveToNeededItem();
    };

    $('.all-best-images').masonry({
        itemSelector: '.all-best-images>*',
        columnWidth: 20
    });

    $('.best-images-btn').on('click', function(e) {
        let hiddenImages = document.getElementsByClassName('best-images-hidden');
        for (let i = 0; i < hiddenImages.length; i++) {
            hiddenImages[i].classList.remove('best-images-hidden');
        }
        $('.all-best-images').masonry({
            itemSelector: '.all-best-images>*',
            columnWidth: 20
        });
        e.target.style.display = 'none';
    });

    function makeHigherAuthorPicture() {
        document.querySelector('.personal-photo-clicked').classList.remove('personal-photo-clicked');
        $(newAuthorPicture).addClass('personal-photo-clicked');
    };

    function moveToNeededItem() {
        let width = window.screen.width;
        let list = document.querySelector('.gallery');
        let position = 0;
        let currentElement = document.querySelector('.personal-photo-clicked');
        let feedBackIndex = $(currentElement).index();
        position = -width * feedBackIndex;
        list.style.marginLeft = position + 'px';
    };

    function checkDisplayedImg() {
        let displayedImages = ($('.our-work-images>*:last-child')).index() + 1;
        if ($('.our-work-btn').css(["display"]).display !== 'none') {
            displayedImages = (displayedImages - 1) / 2 + 1;
        };
        return displayedImages;
    };

    function displayImgAsPerCategory() {
        $('.our-work-images>*').css({ display: 'none' });
        let replacementBlock = document.querySelector('.replacement-block');
        replacementBlock.style.display = 'none';
        let category = $('.nav-our-work-item-clicked').text().toLowerCase().replace(/\s+/g, '-');
        let displayedImages = checkDisplayedImg();
        for (let n = 1; n <= displayedImages; n++) {
            if ($(`.our-work-images>*:nth-child(${n})`).attr('data-category') === category) {
                $(`.our-work-images>*:nth-child(${n})`).css({ display: 'block' });
            } else if (category === 'all') {
                $(`.our-work-images>*:nth-child(${n})`).css({ display: 'block' });
            }
        }
        return category;
    }
})