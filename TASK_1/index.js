const ls = [51, 56, 58, 59, 61];
const t = 174;
const k = 3;

const chooseOptimalDistance = (t, k, ls) => {
    if (checkLsKeys(ls) && checkMaxDistance(t) && checkNumberOfCities(k) && k <= ls.length) {
        const sums = getCombinationSums(ls, k);
        const optimalValue = getOptimalValue(sums, t);
        return optimalValue;
    }
    return null;
}

console.log(chooseOptimalDistance(174, 3, [51, 56, 58, 59, 61])); //173
console.log(chooseOptimalDistance(163, 3, [50])); // null

function checkLsKeys(ls) {
    if (Array.isArray(ls) && ls.length) {
        let newLs = ls.filter(lsValue => !(Number.isInteger(lsValue) && lsValue >= 0));
        return !newLs.length;
    }
    return false;
};

function checkMaxDistance(t) {
    if (Number.isInteger(t) && t >= 0) return true;
    return false;
}

function checkNumberOfCities(k) {
    if (Number.isInteger(k) && k >= 1) return true;
    return false;
}


function getCombinationSums(array, length) {
    function iter(index, right) {
        if (right.length === length) return result.push(right);
        if (index === array.length) return;
        for (let i = index, l = array.length - length + right.length + 1; i < l; i++) {
            iter(i + 1, [...right, array[i]]);
        }
    }
    let combinationSums = [];
    let result = [];
    iter(0, []);
    result.forEach(combination => {
        let sum = combination.reduce((total, n) => {
            return total += n;
        }, 0)
        combinationSums.push(sum)
    })
    return combinationSums;
}

function getOptimalValue(arr, maxVal) {
    let maxValueInArray = Math.max(...arr);
    if (maxValueInArray <= maxVal) return maxValueInArray;
    arr.splice(arr.indexOf(maxValueInArray), 1);
    return getOptimalValue(arr, maxVal);
}