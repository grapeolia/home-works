import gulp from 'gulp';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);
import clean from 'gulp-clean';
import concat from 'gulp-concat';
import del from 'del';
import sourcemaps from 'gulp-sourcemaps';
import watch from 'gulp-watch';
import autoprefixer from 'gulp-autoprefixer';
import cleanCSS from 'gulp-clean-css';
import uglify from 'gulp-uglify';
import imageMin from 'gulp-imagemin';
import sync from 'browser-sync';
const browserSync = sync.create();

gulp.task('delDistFiles', function() {
    return del('dist/**/*.*');
});

gulp.task('styles', function() {
    return gulp
        .src('./src/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer('last 99 versions'))
        .pipe(concat('style.min.css'))
        .pipe(cleanCSS())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream())
})

gulp.task('scripts', function() {
    return gulp
        .src('./src/script/**/*.js')
        .pipe(sourcemaps.init())
        .pipe(concat('index.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./dist/script'))
        .pipe(browserSync.stream())
})

gulp.task('imagesMin', function() {
    return gulp
        .src(['./src/images/**/*.png', './src/images/**/*.svg', './src/images/**/*.jpec', './src/images/**/*.png'])
        .pipe(imageMin())
        .pipe(gulp.dest('./dist/images'))
        .pipe(browserSync.stream())
})

gulp.task('dev', function() {
    browserSync.init({
        server: "./"
    });
    gulp.watch('./index.html').on('change', browserSync.reload);
    gulp.watch('./src/**/*.js', gulp.series('scripts'));
    gulp.watch(['./src/**/*.scss', './src/**/*.js'], gulp.series('styles'));
    gulp.watch(['./src/images/**/*.png', './src/images/**/*.svg', './src/images/**/*.jpec', './src/images/**/*.png'], gulp.series('imagesMin'));

})

gulp.task('browserSync', function() {

})

gulp.task('build', gulp.series(['delDistFiles', gulp.parallel(['styles', 'scripts', 'imagesMin'])]));
gulp.task('default', gulp.series(['build', 'dev']));