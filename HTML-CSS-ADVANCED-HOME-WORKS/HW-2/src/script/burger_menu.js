let burger = document.querySelector(".burger");
let burgerDash = document.querySelectorAll('.burger__dash');
let burgerMenu = document.querySelector('.burger-menu');
burger.addEventListener("click", () => {
    burgerDash.forEach(element => {
        element.classList.toggle('burger__dash-active');
    });
    burgerMenu.classList.toggle('burger-menu_visible');
});