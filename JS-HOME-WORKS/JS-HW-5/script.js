function createNewUser() {
    let newUser = {
        'firstName': prompt('Enter your first name'),
        'lastName': prompt('Enter your last name'),
        'birthday': prompt('Enter your date of birth: DD.MM.YYYY'),

        getAge() {

            let birthArr = this.birthday.split('.');
            let birthFormat = new Date(birthArr[2], birthArr[1] - 1, birthArr[0]);

            const now = new Date();

            while (Number.isNaN(+birthArr[0]) || Number.isNaN(+birthArr[1]) || Number.isNaN(+birthArr[2]) || birthFormat > now) {
                this.birthday = prompt(`Check your DOB: ${this.birthday}. Please enter again DD.MM.YYYY`);
                birthArr = this.birthday.split('.');
                birthFormat = new Date(birthArr[2], birthArr[1] - 1, birthArr[0]);
            };

            let age = now.getFullYear() - birthFormat.getFullYear();

            if ((now.getMonth() < birthFormat.getMonth()) ||
                ((now.getMonth() === birthFormat.getMonth()) && (now.getDate() < birthFormat.getDate()))) {
                age = age - 1;
            };

            return age;
        },

        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },

        getPassword() {
            return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6, 10));
        },

        setNewFirstName(newFirstName) {
            return Object.defineProperty(this, 'firstName', {
                value: newFirstName,
            });
        },
        setNewLastName(newLastName) {
            return Object.defineProperty(this, 'lastName', {
                value: newLastName,
            });
        },
    };

    while (newUser.firstName.trim() === '' || newUser.firstName.match(/\d/)) {
        alert(`First name is incorrect ${newUser.firstName}`);
        newUser.firstName = prompt('Enter your first name again');
    };

    while (newUser.lastName.trim() === '' || newUser.lastName.match(/\d/)) {
        alert(`Last name is incorrect ${newUser.lastName}`);
        newUser.lastName = prompt('Enter your last name again');
    };

    Object.defineProperties(newUser, {
        'firstName': {
            writable: false,
            configurable: true,
        },
        'lastName': {
            writable: false,
            configurable: true,
        },
        'login': {
            value: newUser.getLogin(),
            writable: false,
        },
        'age': {
            value: newUser.getAge(),
            writable: false,
        },
        'password': {
            value: newUser.getPassword(),
            writable: false,
        },
    });

    return newUser;
};

let myUser = createNewUser();

console.log(myUser);
console.log(`${myUser.firstName +' '+ myUser.lastName} is ${myUser.getAge()} y.o.`);
console.log(`${myUser.firstName +' '+ myUser.lastName} has login: ${myUser.getLogin()}`);
console.log(`${myUser.firstName +' '+ myUser.lastName} has password: ${myUser.getPassword()}`);