const icons = document.querySelectorAll('.icon-password');
const passwords = document.querySelectorAll('input');
const confirmButton = document.querySelector('.btn');

icons.forEach(item => {
    item.addEventListener('mousedown', (e) => {
        showOrHidePassword(e, 'show');
    })
    item.addEventListener('mouseup', (e) => {
        showOrHidePassword(e, 'hide');
    })
});

passwords.forEach(input => {
    input.onclick = deleteErrorMessage;
});

confirmButton.addEventListener('click', () => {
    if ((passwords[0].value === passwords[1].value) && passwords[0].value.trim() !== '') {
        alert('You are welcome!');
    } else {
        generateErrorMessage();
    }
});

function showOrHidePassword(e, event) {
    const clickedLabel = e.target.closest('label');
    const clickedIcons = clickedLabel.getElementsByTagName('i');
    const input = clickedLabel.getElementsByTagName('input')[0];

    if (event === 'show') {
        clickedIcons[0].classList.add('hidenelem');
        clickedIcons[1].classList.remove('hidenelem');
        input.type = 'text';
    } else if (event === 'hide') {
        clickedIcons[0].classList.remove('hidenelem');
        clickedIcons[1].classList.add('hidenelem');
        input.type = 'password';
    };
};

function generateErrorMessage() {
    let error = document.querySelector('[data-message]');
    if (!error) {
        error = document.createElement('p');
        error.dataset.message = 'errorMessage';
        error.innerText = 'Нужно ввести одинаковые значения';
        error.style.color = 'red';
        confirmButton.after(error);
    };
};

function deleteErrorMessage() {
    const error = document.querySelector('[data-message]');
    if (error) {
        error.remove();
    };
};