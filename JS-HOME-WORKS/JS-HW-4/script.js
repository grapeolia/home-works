function createNewUser() {
    let newUser = {
        'firstName': prompt('Enter your first name'),
        'lastName': prompt('Enter your last name'),

        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        setNewFirstName(newFirstName) {
            return Object.defineProperty(this, 'firstName', {
                value: newFirstName,
            });
        },
        setNewLastName(newLastName) {
            return Object.defineProperty(this, 'lastName', {
                value: newLastName,
            });
        },
    };

    while (newUser.firstName.trim() === '' || newUser.firstName.match(/\d/)) {
        alert(`First name is incorrect ${newUser.firstName}`);
        newUser.firstName = prompt('Enter your first name again');
    };

    while (newUser.lastName.trim() === '' || newUser.lastName.match(/\d/)) {
        alert(`Last name is incorrect ${newUser.lastName}`);
        newUser.lastName = prompt('Enter your last name again');
    };

    Object.defineProperties(newUser, {
        'firstName': {
            writable: false,
            configurable: true,
        },
        'lastName': {
            writable: false,
            configurable: true,
        },
        'login': {
            value: newUser.getLogin(),
        },
    });

    return newUser;

};

let myUser = createNewUser();

console.log(myUser);
console.log(myUser.login);