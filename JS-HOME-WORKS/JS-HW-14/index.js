$(document).ready(function() {
    $('.page-menu-item a').on('click', function(e) {
        e.preventDefault();
        let id = $(this).attr('href');
        let scrollTo = $(id).offset().top;
        console.log(scrollTo);
        $('html, body').stop().animate({
            scrollTop: scrollTo
        }, scrollTo);
    });

    $('.hide-section-btn').click(function() {
        $('.hot-news').slideToggle(), 2000
    });

    $(window).on('scroll', function() {
        if ($(this).scrollTop() > 1) {
            if ($('#scroll-up-btn').is(':hidden')) {
                $('#scroll-up-btn').css({ visibility: 'visible' }).fadeIn('slow');
            }
        } else {
            $('#scroll-up-btn').stop(true, false).fadeOut('fast');
        }
    });

    $('#scroll-up-btn').click(function() {
        $("html, body").animate({ scrollTop: 0 }, 1000);
        return false;
    });
});