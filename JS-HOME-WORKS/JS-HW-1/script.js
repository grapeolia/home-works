//const - оголошуються змінні, яким не можна тоді присвоїти нове значення
//let, var - оголошуємо змінні, значення яких можна змінювати
//var - при вживанні var в блоці (н-д, if) змінюється значення var і за межами блока
//вживання let дозволяє працювати із змінною в блоці, при цьому не впливати на змінну let за межами блока
//var - при вживанні var в блоці (н-д, if) змінюється значення var і за межами блока, що може бути незручно


let usersName = '';
let age = 0;

do {
    usersName = prompt('Please enter your name', `${usersName}`);
} while (usersName.match(/\d/) || !usersName === !!null || usersName === '');

do {
    age = +prompt('Please enter your age (1-100 y.o)', `${age}`);
} while (Number.isNaN(age) || age <= 0 || age > 99);

if (age < 18) {
    alert('You are not allowed to visit this website');
} else if (age >= 18 && age <= 22) {
    if (confirm('Are you sure you want to continue?')) {
        alert(`Welcome, ${usersName}`);
    } else {
        alert('You are not allowed to visit this website');
    }
} else {
    alert(`Welcome, ${usersName}`);
}