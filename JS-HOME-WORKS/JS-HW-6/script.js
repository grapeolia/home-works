let array = ['hello', 'world', 23, '23', true, null, , { objKey: 'objValue', },
    ['arrElem1', 'arrElem2'],
];
const allTypes = ['string', 'number', 'boolean', 'object', 'undefined', 'null'];

console.log(allTypes);
console.log(array);

function filterByFilterMethod(array, dataType) {
    return array.filter(element => (typeof element !== dataType && dataType !== 'null') ||
        (dataType === 'object' && element === null) ||
        (dataType === 'null' && element !== null));
};

allTypes.forEach(type => console.log(filterByFilterMethod(array, type)));

function filterByForEachMethod(array, dataType) {
    let newArrayForEach = [];
    array.forEach(element => {
        if ((typeof element !== dataType && dataType !== 'null') ||
            (dataType === 'object' && element === null) ||
            (dataType === 'null' && element !== null) ||
            (dataType !== 'undefined' && element === '')) {
            newArrayForEach.push(element);
        };

        return newArrayForEach;
    });
};

allTypes.forEach(type => console.log(filterByForEachMethod(array, type)));

function filterByLoopMethod(array, dataType) {
    let arrayLoop = [];
    for (let i = 0; i < array.length; i++) {
        if (typeof array[i] !== dataType && dataType !== 'null') {
            arrayLoop.push(array[i]);
        };
        if (dataType === 'object' && array[i] === null) {
            arrayLoop.push(array[i]);
        };
        if (dataType === 'null' && array[i] !== null) {
            arrayLoop.push(array[i]);
        };
    };
    return arrayLoop;
};

allTypes.forEach(type => console.log(filterByLoopMethod(array, type)));