const tabs = document.getElementById('tabs');
const contentItems = makeContentItemsWithDataAttr();

tabs.addEventListener('click', (e) => {
    e.stopPropagation();
    const itemHeading = e.target;
    let contentToShow = '';
    for (let i = 0; i < contentItems.length; i++) {
        if (contentItems[i].dataset.heading === itemHeading.innerText) {
            contentToShow = contentItems[i];
        }
        tabs.children[i] === itemHeading ? tabs.children[i].classList.add('active') : tabs.children[i].classList.remove('active');
        contentItems[i] === contentToShow ? contentItems[i].className = 'tabsvisible' : contentItems[i].className = 'tabsnotvisible';
    };
});

function createArrHeadings(tabsContent) {
    const allChildrenNodes = tabsContent.childNodes;
    let headings = [];
    for (let i = 0; i < allChildrenNodes.length; i++) {
        if (allChildrenNodes[i].nodeType === Node.COMMENT_NODE) {
            headings.push(allChildrenNodes[i].data);
        };
    };
    return headings;
}

function makeContentItemsWithDataAttr() {
    const tabsContent = document.getElementById('tabs-content');
    const headings = createArrHeadings(tabsContent);
    const contentItems = tabsContent.children;
    for (let i = 0, y = 0; i < headings.length, y < contentItems.length; i++, y++) {
        contentItems[y].dataset.heading = headings[i];
        contentItems[y].classList.add('tabsnotvisible');
    };
    return contentItems;
}