const allPrices = document.createElement('p');
const label = document.createElement('label');
const input = document.createElement('input');

input.setAttribute('id', 'price');
label.setAttribute('for', 'price');

document.body.insertAdjacentElement('afterbegin', allPrices);
allPrices.insertAdjacentElement('afterend', label);
label.insertAdjacentElement('afterend', input);

allPrices.innerText = 'Current Prices: ';
label.innerText = 'Price, $: ';

input.addEventListener('focusin', () => {
    input.value = '';
    input.className = 'focusin';
});

input.addEventListener('focusout', () => {
    input.classList.remove('focusin');

    if (!Number.isNaN(+input.value) && +input.value > 0) {
        if (!!document.getElementById('error')) { document.getElementById('error').remove() };

        const usersPrice = addUsersPrice();
        const deleteBtn = createDeleteButton();

        allPrices.appendChild(usersPrice);
        usersPrice.appendChild(deleteBtn)

        input.className = 'focusouttrue';
    } else {
        const errorNotification = createErrorNotification();
        input.after(errorNotification);

        input.className = 'focusoutfalse';
    }
});

function addUsersPrice() {
    const usersPrice = document.createElement('span');
    usersPrice.innerText = `$ ${input.value}`;

    return usersPrice;
}

function createDeleteButton() {
    const btn = document.createElement('button');
    btn.innerText = 'X';
    btn.onclick = (e) => {
        e.target.closest('span').remove();
    }
    return btn;
}

function createErrorNotification() {
    let errorNotification = document.getElementById('error');
    if (!errorNotification) {
        errorNotification = document.createElement('p');
        errorNotification.innerText = 'Please enter correct price!';
        errorNotification.setAttribute('id', 'error');
    }
    return errorNotification;
};