document.addEventListener('keydown', (e) => {
    const buttons = document.querySelectorAll('button');
    const pressedBtn = e.key.toLowerCase();
    for (let i = 0; i < buttons.length; i++) {
        pressedBtn === buttons[i].innerText.toLocaleLowerCase() ?
            buttons[i].style.backgroundColor = 'blue' :
            buttons[i].style.backgroundColor = 'black';
    }
});