const images = document.querySelectorAll('.image-to-show');
const pauseBtn = document.getElementById('pauseBtn');
const contBtn = document.getElementById('continueBtn');


function showImg(i = 0) {
    i = i === images.length ? 0 : i;
    let img = images[i];
    let prevImg = i > 0 ? img.previousElementSibling : images[images.length - 1];
    if (!!prevImg) {
        prevImg.classList.add('fadeOut');
    };

    img.classList.add('fadeIn');
    img.style.visibility = 'visible';
    img.addEventListener('animationend', () => {
        img.classList.remove('fadeIn');
        prevImg.classList.remove('fadeOut');
        prevImg.style.visibility = 'hidden';
    });

    i++;

    const cycleShowing = setTimeout(() => {
        showImg(i);
    }, 3000);

    pauseBtn.addEventListener('click', () => {
        clearTimeout(cycleShowing);
    });

    return i;
};

contBtn.addEventListener('click', () => {
    for (let i = 0; i < images.length; i++) {
        if (images[i].style.visibility === 'visible') {
            showImg(i);
        };
    };
});

showImg();