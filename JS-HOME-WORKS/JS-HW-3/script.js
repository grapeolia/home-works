function mathOperation() {
    let m = prompt('Enter your first number');
    let n = prompt('Enter your second number');

    while (Number.isNaN(+m) || m.trim() === '' || Number.isNaN(+n) || n.trim() === '') {
        alert('Your numbers do not match conditions. Please enter again');
        m = prompt('Enter your first number', `${m}`);
        n = prompt('Enter your second number', `${n}`);
    };

    let operation = prompt(`Enter mathematical operation which needs to be done: +, -, *, /`);

    while (operation !== '+' && operation !== '-' && operation !== '*' && operation !== '/') {
        alert(`${operation} is not mathematical operation. Please enter again`);
        operation = prompt(`Enter mathematical operation which needs to be done: +, -, *, /`);
    };

    m = +m;
    n = +n;

    let mathResult = 0;

    switch (operation) {
        case '+':
            mathResult = m + n;
            break;
        case '-':
            mathResult = m - n;
            break;
        case '/':
            mathResult = m / n;
            break;
        case '*':
            mathResult = m * n;
            break;
    }
    alert(`${m} ${operation} ${n} = ${mathResult}`);
}

<<<<<<< HEAD
mathOperation();
=======
mathOperation();
>>>>>>> c1591256454d0e3f88a0227a3a0808ac172fc2a7
