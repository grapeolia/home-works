let m = prompt(`Enter your less number (natural only)`);
let n = prompt(`Enter your bigger number (natural only)`);
let numbersMultFive = '';
let compositeNum = '' + 0 + 1;
let primeNumbers = '';

while (Number.isNaN(+m) || m.trim() === '' || !Number.isInteger(+m) ||
    Number.isNaN(+n) || n.trim() === '' || !Number.isInteger(+n) || m < 0 || n < 0 || m > n) {
    alert('Your numbers do not match conditions. Please enter again');
    m = prompt('Enter your less number (natural only) again', `${m}`);
    n = prompt('Enter your bigger number (natural only)', `${n}`);
};


for (let i = m; i <= n; i++) {
    for (let y = 2; y < n; y++) {
        while (i > y) {
            if (!(i % y)) {
                compositeNum += i + ' ';
            }
            break;
        }
    }
    if (!compositeNum.includes(i)) {
        primeNumbers += i + ' ';
    }
    if (!(i % 5)) {
        numbersMultFive = numbersMultFive + i + ' ';
    }
}

console.log(`Prime numbers from ${m} to ${n}: ${primeNumbers}`);
console.log(`Numbers multiple of five from ${m} to ${n}: ${numbersMultFive}`);