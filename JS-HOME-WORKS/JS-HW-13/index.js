const btnThemeChange = document.querySelector('.theme-change');
const elements = document.querySelectorAll('body, body *');
const darkClasses = ['main-menu', 'main-menu-item', 'aside-menu', 'aside-menu-item',
    'image', 'text', 'footer', 'footer-menu-item', 'author-rights', 'theme-change',
    'main-title'
];

btnThemeChange.addEventListener('click', () => {
    changeTheme();
});

document.addEventListener('DOMContentLoaded', () => {
    if (localStorage['theme'] === 'dark') {
        changeTheme();
    };
});

function changeTheme() {
    document.body.classList.toggle('body-dark');
    for (let i = 0; i < elements.length; i++) {
        for (let y = 0; y < darkClasses.length; y++) {
            if (!!elements[i].classList.contains(`${darkClasses[y]}`)) {
                elements[i].classList.toggle(`${darkClasses[y]}-dark`)
            };
        };
    };
    if (!!document.body.classList.contains('body-dark')) {
        localStorage['theme'] = 'dark';
        btnThemeChange.innerText = 'Light Theme';
    } else {
        localStorage['theme'] = 'light';
        btnThemeChange.innerText = 'Dark Theme';
    }
};